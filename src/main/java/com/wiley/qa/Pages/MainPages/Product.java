package com.wiley.qa.Pages.MainPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class Product {

    public boolean checkTitleProduct (WebDriver driver){
        List<WebElement>elements = driver.findElements(By.cssSelector(".products-list span.search-highlight"));
        for (WebElement element : elements){
            if (element.getAttribute("innerHTML").contains("Java")){
                continue;
            }else {
                return false;
            }
        }
        return true;
    }

    public List<WebElement> checkCountProductOnPage (WebDriver driver){
        return driver.findElements(By.cssSelector(".products-list .product-item"));
    }

    public List<WebElement> checkAddToCardOnProduct (WebDriver driver){
        return driver.findElements(By.xpath("//section[@class='product-item '][div[@class='product-content']//form[@class='add_to_cart_form']]"));
    }



}
