package com.wiley.qa.Pages.MainPages.wileyplus.com;

import com.wiley.qa.Pages.MainPage;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class MainPageWileyPlus extends MainPage {

    public MainPageWileyPlus(WebDriver driver) {
        super(driver);
    }


    @Override
    public boolean getURL(WebDriver driver) {
        try {
            TimeUnit.SECONDS.sleep(3);
        }catch (Exception e){

        }
        return driver.getCurrentUrl().equals("https://www.wileyplus.com/");
    }
}
