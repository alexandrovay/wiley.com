package com.wiley.qa.Pages.MainPages.Subjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.*;

public class Education {

    public List<String> dateForLeftSide (WebDriver driver){
        List<String>nameElements = new LinkedList<String>();
        nameElements.add("Information & Library Science");
        nameElements.add("Education & Public Policy");
        nameElements.add("K-12 General");
        nameElements.add("Higher Education General");
        nameElements.add("Vocational Technology");
        nameElements.add("Conflict Resolution & Mediation (School settings)");
        nameElements.add("Curriculum Tools- General");
        nameElements.add("Special Educational Needs");
        nameElements.add("Theory of Education");
        nameElements.add("Education Special Topics");
        nameElements.add("Educational Research & Statistics");
        nameElements.add("Literacy & Reading");
        nameElements.add("Classroom Management");
        return nameElements;
    }

    public List<String> getLeftSideElement (WebDriver driver){
        List<String>nameElements = dateForLeftSide(driver);
        List<WebElement> elements = driver.findElements(By.cssSelector(".side-panel a"));
        ListIterator<String> iterator = nameElements.listIterator();
        while (iterator.hasNext()){
            for (WebElement element : elements){
                if (iterator.hasNext()){
                    String as = iterator.next();
                if (element.getText().equals(as)) {
                    iterator.remove();
                }
                }
            }
        }
        return nameElements;
    }
}


