package com.wiley.qa.Pages.MainPages.WhoWeServe;

import com.wiley.qa.Pages.MainPage;
import com.wiley.qa.Pages.MainPages.wileyplus.com.MainPageWileyPlus;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class StudentsPage extends MainPage {

    public StudentsPage(WebDriver driver) {
        super(driver);
    }
    @Override
    public boolean getURL(WebDriver driver) {
        return driver.getCurrentUrl().equals("https://www.wiley.com/en-ru/students");
    }

    public boolean learnMoreBeSmart (WebDriver driver){
        driver.findElement(By.cssSelector("a[href='https://www.wileyplus.com/']")).click();
        return new MainPageWileyPlus(driver).getURL(driver);
    }
}
