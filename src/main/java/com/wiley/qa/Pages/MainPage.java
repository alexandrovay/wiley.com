package com.wiley.qa.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.*;

public class MainPage {
    WebDriver driver;
    public static String HOME_PAGE_MAIN = "Homepage | Wiley";
    public static String BUTTON_SEARCH = "button.glyphicon-search";
    public static String INPUT_SEARCH = "input[type='search']";




    public MainPage(WebDriver driver) {
        this.driver = driver;
    }

    public List<String> listTitlesWhoWeServe(){
        List<String>list = new LinkedList<String>();
        list.add("Students");
        list.add("Instructors");
        list.add("Book Authors");
        list.add("Professionals");
        list.add("Researchers");
        list.add("Institutions");
        list.add("Librarians");
        list.add("Corporations");
        list.add("Societies");
        list.add("Journal Editors");
        list.add("Government");
        return list;
    }


    public boolean checkSubHeaderElement (WebDriver driver,WebElement getElement){
        boolean check = false;
        List<String>list = listTitlesWhoWeServe();
        Actions actions = new Actions(driver);
        actions.moveToElement(getElement).build().perform();
        List<WebElement>elements = driver.findElements(By.cssSelector("ul.ps-theme-default li.dropdown-item"));
        for (String elem : list){
            for (WebElement element : elements){
                if (elem.equals(element.getText())){
                    check = true;
                    break;
                }
                else {
                    check=false;
                }
            }
        }
        return check;
    }


    public boolean getHeader (WebDriver driver){
        return driver.findElement(By.cssSelector(".active")).isDisplayed();
    }

    public boolean getURL (WebDriver driver){
        return driver.getCurrentUrl().equals("https://www.wiley.com/en-ru");
    }


    public WebElement whoWeServe (WebDriver driver){
       return driver.findElement(By.xpath("//div[@id='Level1NavNode1']/../a"));
    }

    public WebElement subjects (WebDriver driver) {
        return driver.findElement(By.cssSelector(".dropdown-submenu a[href*='/subjects']"));
    }

    public WebElement about (WebDriver driver) {
        return driver.findElement(By.xpath("//div[@id='Level1NavNode4']/../a"));
    }

    public void goToEducationPage(WebDriver driver){
        Actions actions = new Actions(driver);
        actions.moveToElement(subjects(driver)).build().perform();
        driver.findElement(By.cssSelector("li.dropdown-submenu a[href='/en-ru/Education-c-ED00']")).click();
    }


//    public List<String> whoWeServeContains (WebDriver driver){
//        List<String>list = listTitlesWhoWeServe();
//        Actions actions = new Actions(driver);
//        actions.moveToElement(whoWeServe(driver)).build().perform();
//        List<WebElement>elements = driver.findElements(By.cssSelector("ul.ps-theme-default li.dropdown-item"));
//        return validationList(list,elements);
//    }
//
//    public List<String>validationList(List<String> list,List<WebElement>elements){
//        String a;
//        ListIterator<String> iterator = list.listIterator();
//        while (iterator.hasNext()) {
//            for (WebElement element : elements) {
//                if (iterator.hasNext()){
//                    a = iterator.next();
//                    if (element.getText().equals(a)) {
//                        iterator.remove();
//                    }else {
//
//                    }
//                }
//            }
//
//        }
//        return list;
//    }


    public List<WebElement> getSubHeader_WhoWeServe (WebDriver driver){
        Actions actions = new Actions(driver);
        actions.moveToElement(whoWeServe(driver)).build().perform();
        return driver.findElements(By.cssSelector("ul.ps-theme-default li.dropdown-item"));
    }

    public  Object goToSubHeaderForWhoWeServe (String subHeader, WebDriver driver, Object object){
        List<WebElement>elements = getSubHeader_WhoWeServe(driver);
        for (WebElement element : elements){
            if (element.getText().equals(subHeader)){
                element.click();
                return object;
            }
        }
        return false;
    }


    public boolean checkDisplayedTextUnderSearch_Suggestions(WebDriver driver){
        List<WebElement>elements = driver.findElements(By.cssSelector("aside.ui-autocomplete .search-related-content span.search-highlight"));
        for (WebElement element : elements){
           if (element.getAttribute("innerHTML").startsWith("java")){
                continue;
           }else {
                return false;
            }
        }
        return true;
    }

    public boolean checkDisplayedTextUnderSearch_Products (WebDriver driver){
        List<WebElement>elements = driver.findElements(By.cssSelector("aside.ui-autocomplete section.related-content-products-section span.search-highlight"));
        for (WebElement element : elements){
            if (element.getAttribute("innerHTML").contains("Java")){
                continue;
            }else {
                return false;
            }
        }
        return true;
    }

    public List<String> getListStringFromListWebElement (String loc, WebDriver driver){
        List<WebElement> elements = driver.findElements(By.cssSelector(loc));
        List<String>stringList = new ArrayList<String>();
        for (WebElement element : elements){
            stringList.add(element.getText());
        }
        return stringList;
    }
}
