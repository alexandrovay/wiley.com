package com.wiley.qa;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.io.FileOutputStream;
import java.io.IOException;

public class Steps {
    final static private String BASE_URL = "https://httpbin.org";


    public static Response req (String basePath){
        RequestSpecification request = RestAssured
                .given()
                .log().all();
        return request.baseUri(BASE_URL)
                .basePath(basePath)
                .request(Method.GET)
                .then()
                .log().all().extract()
                .response();
    }

    public static void saveImage (Response response){
        byte[] bytes = response.asByteArray();
        try {
            FileOutputStream fos = new FileOutputStream("C://Users//Admin//Downloads//newImage.png");
            fos.write(bytes);
        }catch (IOException e){

        }

    }
}
