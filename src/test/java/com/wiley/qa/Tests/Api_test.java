package com.wiley.qa.Tests;

import com.wiley.qa.Steps;
import io.restassured.response.Response;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

public class Api_test {

    @Test
    public void test1 () {
        Response response = Steps.req("/delay/0");
        assertThat(response.statusCode()).as("Получен неккоректный статус код").isEqualTo(200);
    }

    @Test
    public void test2 (){
        Response response = Steps.req("/image/png");
        Steps.saveImage(response);
        assertThat(response.statusCode()).as("Получен неккоректный статус код").isEqualTo(200);
    }



}

