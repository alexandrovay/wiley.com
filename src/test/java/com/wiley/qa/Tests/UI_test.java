package com.wiley.qa.Tests;

import com.wiley.qa.Pages.MainPage;
import com.wiley.qa.Pages.MainPages.Product;
import com.wiley.qa.Pages.MainPages.Subjects.Education;
import com.wiley.qa.Pages.MainPages.WhoWeServe.StudentsPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class UI_test {

    private WebDriver driver;
    MainPage mainPage = new MainPage(driver);
    StudentsPage studentsPage = new StudentsPage(driver);
    Education education = new Education();
    Product product = new Product();

    @Before
    public void setDriver (){
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://www.wiley.com/en-ru#");

    }

    @After
    public void closeDriver (){
        driver.close();
    }

    @Test
    public void oneTest (){
        mainPage.whoWeServe(driver).isDisplayed();
        mainPage.subjects(driver).isDisplayed();
        mainPage.about(driver).isDisplayed();
    }

    @Test
    public void secondTest (){
        assertThat(mainPage.checkSubHeaderElement(driver,
                mainPage.whoWeServe(driver)))
                .isTrue();
    }

    @Test
    public void threeTest (){
        mainPage.goToSubHeaderForWhoWeServe("Students",driver, StudentsPage.class);
        assertThat(studentsPage.getURL(driver)).isTrue();
        assertThat(studentsPage.getHeader(driver)).isTrue();
        assertThat(studentsPage.learnMoreBeSmart(driver)).isTrue();
    }

    @Test
    public void fourTest (){
        mainPage.goToEducationPage( driver);
        assertThat(driver.getCurrentUrl()).isEqualTo("https://www.wiley.com/en-ru/Education-c-ED00");
        assertThat(mainPage.getHeader(driver)).isTrue();
        assertThat(education.getLeftSideElement(driver).size()).isEqualTo(0);
    }

    @Test
    public void fiveTest (){
        driver.findElement(By.cssSelector("a[href='/en-ru/']")).click();
        assertThat(driver.getTitle().equals(MainPage.HOME_PAGE_MAIN));
    }

    @Test
    public void sixTest (){
        driver.findElement(By.cssSelector(MainPage.BUTTON_SEARCH)).click();
        assertThat(driver.getTitle().equals(MainPage.HOME_PAGE_MAIN));
    }

    @Test
    public void sevenTest (){
        driver.findElement(By.cssSelector(MainPage.INPUT_SEARCH)).sendKeys("java");
        assertThat(mainPage.checkDisplayedTextUnderSearch_Suggestions(driver)).isTrue();
        assertThat(mainPage.checkDisplayedTextUnderSearch_Products(driver)).isTrue();
    }

    @Test
    public void eigthtTest (){
        driver.findElement(By.cssSelector(MainPage.INPUT_SEARCH)).sendKeys("java");
        driver.findElement(By.cssSelector(MainPage.BUTTON_SEARCH)).click();
        assertThat(product.checkTitleProduct(driver)).isTrue();
        assertThat(product.checkCountProductOnPage(driver).size())
                .as("Количество элеменентов на странице не равно 10")
                .isEqualTo(10);
        assertThat(product.checkAddToCardOnProduct(driver).size())
                .as("Не на всех картах присутствует элемент \"ADD TO CARD\"")
                .isEqualTo(10);
    }

    @Test
    public void ninthTest (){
        String getTitlesProductFromRelultPage = ".products-list span.search-highlight";
        driver.findElement(By.cssSelector(MainPage.INPUT_SEARCH)).sendKeys("java");
        driver.findElement(By.cssSelector(MainPage.BUTTON_SEARCH)).click();
        List<String>listNameOne = mainPage.getListStringFromListWebElement(getTitlesProductFromRelultPage, driver);
        driver.findElement(By.cssSelector(MainPage.INPUT_SEARCH)).clear();
        driver.findElement(By.cssSelector(MainPage.INPUT_SEARCH)).sendKeys("java");
        driver.findElement(By.cssSelector(MainPage.BUTTON_SEARCH)).click();
        List<String>listNameSecond = mainPage.getListStringFromListWebElement(getTitlesProductFromRelultPage, driver);
        assertThat(listNameOne).isEqualTo(listNameSecond);
    }
}
