package com.wiley.qa.Tests;

import com.wiley.qa.Steps;
import io.restassured.response.Response;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import static org.assertj.core.api.Assertions.*;

import java.util.Arrays;

@RunWith(Parameterized.class)
public class ParameterizedTest {
   private String str;

    public ParameterizedTest(String str) {
        this.str = str;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> dataForTest() {
        return Arrays.asList(new Object[][]{
                {"1"},
                {"2"},
                {"3"},
                {"4"},
                {"5"},
                {"6"},
                {"7"},
                {"8"},
                {"9"},
                {"10"},
        });
    }

    @Test
    public void test (){
        Response response = Steps.req(String.format("/delay/%s",str));
        assertThat(response.statusCode()).as("Некорректный статус код").isEqualTo(200);
    }
}
